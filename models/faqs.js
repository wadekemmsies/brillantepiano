var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var faqsSchema = new mongoose.Schema({
    question: String,
    answer: String
});

faqsSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("faqs", faqsSchema);