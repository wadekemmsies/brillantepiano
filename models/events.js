var mongoose = require("mongoose");

var dateSchema = new mongoose.Schema({
    date: String,
    event: String
});

module.exports = mongoose.model("Date", dateSchema);