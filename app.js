var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var methodOverride = require("method-override");
var User = require("./models/user");
var dates = require("./models/events");
var Faqs = require("./models/faqs");

mongoose.connect("mongodb+srv://Kemmsies:*password@cluster0-2wg4p.mongodb.net/brilliante_piano?retryWrites=true", {useNewUrlParser: true});
var app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));

app.use(require("express-session")({
    secret: "Buddy is the best and cutest dog in the world",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next){
    res.locals.currentUser = req.user;
    next();
});

// Faqs.create({
//     question: "Why do you like to suck the pp",
//     answer: "because you're a fat loser"
// }, function(err, faq) {
//     if (err) {
//         console.log(err);
// }   else {
// console.log('Saved: ');
// console.log(faq);
// }
// });


app.get("/", function(req, res){
    res.render("home");
});

app.get("/home", function(req, res){
    res.render("home");
});

app.get("/about", function(req, res){
    res.render("about"); 
});

app.get("/tuition", function(req, res){
    res.render("tuition"); 
});

app.get("/admin", function(req, res){
    res.render("adminLogin");     
});

app.get("/faqs", function(req, res){
    Faqs.find({}, function(err, allFaqs){
        if(err){
            console.log(err);
        } else {
            res.render("faqs", {faqs:allFaqs, currentUser: req.user});
        }
    });
});

// app.get("/faqs/new", function(req, res){
//     res.render("newfaq"); 
// });

// app.post("/faqs", function(req, res){
//     Faqs.create(req.body.faq, function(err, newFaq){
//         if(err){
//             console.log(err);
//         } else {
//             res.redirect("/faqs");
//         }
//     });
// });

app.get("/calender", function(req, res){
    dates.find({}, function(err, allDates){
       if(err){
           console.log(err);
       } else {
          res.render("calender", {dates:allDates, currentUser: req.user});
       }
    });
});

app.get("/calender/new", function(req, res){
    res.render("newEvent");
});

app.post("/calender", function(req, res){
    dates.create(req.body.date, function(err, newDate){
       if(err) {
           console.log(err);
       } else {
           res.redirect("/calender");
       }
    });
});

app.get("/calender/:id/edit", function(req, res){
    dates.findById(req.params.id, function(err, foundEvent){
    if(err){
        console.log(err);
    }
    res.render("editEvent", {date: foundEvent});
    });
});

app.put("/calender/:id/", function(req, res){
    dates.findByIdAndUpdate(req.params.id, req.body.date, function(err, updatedEvent){
        if(err){
            console.log(err);
        } else {
            res.redirect("/calender");
        }
    });
});

app.delete("/calender/:id/", function(req, res){
    dates.findByIdAndRemove(req.params.id, function(err){
        if(err){
            console.log(err);
        } else {
            res.redirect("/calender");
        }
    });
});


app.get("/registeradministrator", function(req, res){
     res.render("register");
});


app.post("/register", function(req, res){
    User.register(new User({username: req.body.username}), req.body.password, function(err, user){
        if(err){
            console.log(err);
            return res.render("register");
        }
        passport.authenticate("local")(req, res, function(){
           res.redirect("/home");
        });
    });
});

app.get("/login", function(req, res){
    res.render("adminLogin");
});

app.post("/login", passport.authenticate("local", 
{
    successRedirect: "/home",
    failureRedirect: "/login"
}), function(req, res){
    
});

app.get("/logout", function(req, res){
    req.logout();
    res.redirect("/home");
});

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
}

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("The server has started!");
});
